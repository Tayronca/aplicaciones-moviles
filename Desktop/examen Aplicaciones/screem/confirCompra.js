import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native'



class confirCompra extends Component {
    constructor(props) {
        super(props);

        this.state = {
            lista: this.props.listaCarro
        }
    }

    componentWillUpdate(nextProps, nextState) {
  


            nextState.lista =nextProps.listaCarro

       
    }

    removeItem=(e)=>{

        this.props.remove(e)
    }

    render() {


        var total = 0;

        this.state.lista.map(e => {

            total += e.Total
        })

        return (
            <View>
                <View style={styles.containerLista}>
                    {this.state.lista.map((e, index) => {

                        return (
                            <View key={index} style={{ flex: 1, flexDirection: 'row', alignItems: "center", borderBottomWidth: 0.3, borderBottomColor: "#eee" }}>
                                <TouchableOpacity style={{marginHorizontal:10}} onPress={()=>this.removeItem(e)}><Text style={{color:"red",fontWeight:600}}>X</Text></TouchableOpacity>
                                <Text style={{ fontSize: 18, fontWeight: 600, marginVertical: 5, flex: 3 }}>{e.Nombre}(x{e.Cantidad})</Text>
                                <Text style={{ textAlign: 'right', fontWeight: 600, fontSize: 18, flex: 3}}>{e.Total} $</Text>
                            </View>
                        )
                    })}

                    {total > 0 ?
                        <View style={styles.Total}>
                            <Text style={{ textAlign: 'right', fontWeight: 600, fontSize: 20 }}>{total} $</Text>
                        </View>
                        : <Text style={{ textAlign: 'right', fontWeight: 600, fontSize: 20 }}>No hay productos</Text>
                    }

                    <TouchableOpacity style={styles.button} onPress={() => this.propsnavigation.navigate('Envio')}>
                        <Text>Envio</Text>
                    </TouchableOpacity>

                </View>

            </View>
        )
    }
}




export default confirCompra;

const styles = StyleSheet.create({

    button: {
        backgroundColor: "#ff0099",
        padding: 10,
        width: 150
    },
    containerLista: {
        backgroundColor: "#fff",
        padding: 10

    }

})
